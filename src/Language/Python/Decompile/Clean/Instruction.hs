{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Clean.Instruction where

import Data.Bits
import qualified Data.HashSet as HS

import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | Merges EXTENDED_ARG opcodes and turns relative jumps into absolute jumps.
simplifyInstructions :: [Instruction] -> [Instruction]
simplifyInstructions (i1 : i2 : is)
  | opcode i1 == EXTENDED_ARG = simplifyInstructions
    $ i2 { argument = argument i1 `shiftL` 8 + argument i2 } : is
simplifyInstructions (i : is)
  | opcode i `elem` relative
  = i { argument = row i + 2 + argument i } : simplifyInstructions is
  | otherwise
  = i : simplifyInstructions is
simplifyInstructions [] = []

-- | Opcodes with relative targets.
relative :: HS.HashSet OpCode
relative = HS.fromList
  [ FOR_ITER, JUMP_FORWARD
  , SETUP_EXCEPT, SETUP_FINALLY, SETUP_LOOP, SETUP_WITH
  ]
