module Language.Python.Decompile.Clean.Block where

import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS

import Language.Python.Decompile.Block
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | Cleans all blocks.
cleanBlocks :: BlockSeq -> BlockSeq
cleanBlocks = map cleanBlock

-- | Cleans all instructions within a block.
cleanBlock :: Block' -> Block'
cleanBlock b = b { block' = clean $ block' b }
  where clean = \case
          BBasic is -> BBasic $ cleanInstructions is
          BWith bs -> BWith $ cleanBlocks bs
          BExcept tbs ebs -> BExcept (cleanBlocks tbs) (cleanBlocks ebs)
          BFinally tbs fbs -> BFinally (cleanBlocks tbs) (cleanBlocks fbs)
          BIf b' c ibs ebs
            -> BIf b' (cleanCond c) (cleanBlocks ibs) (cleanBlocks ebs)
          BWhile c bs -> BWhile (cleanCond c) $ cleanBlocks bs
          BFor bs -> BFor $ cleanBlocks bs

-- | Cleans all blocks within a condition structure.
cleanCond :: Cond -> Cond
cleanCond = \case
  CBlock b  -> CBlock $ cleanBlock b
  CTrue     -> CTrue
  CNot c    -> CNot   $ cleanCond c
  CAnd c c' -> CAnd (cleanCond c) (cleanCond c')
  COr  c c' -> COr  (cleanCond c) (cleanCond c')

-- | Removes unneeded opcodes and replaces opcodes that can be treated the same.
cleanInstructions :: [Instruction] -> [Instruction]
cleanInstructions [] = []
cleanInstructions (i : is)
  | opcode i `elem` noOps
  = cleanInstructions is
  | Just op <- HM.lookup (opcode i) equivalent
  = i { opcode = op } : cleanInstructions is
  | otherwise
  = i : cleanInstructions is

-- | Opcodes that no longer serve a purpose or that do not impact decompilation.
noOps :: HS.HashSet OpCode
noOps = HS.fromList
  [ END_FINALLY, FOR_ITER, GET_ITER, GET_YIELD_FROM_ITER
  , JUMP_ABSOLUTE, JUMP_FORWARD, JUMP_IF_FALSE_OR_POP
  , POP_BLOCK, POP_EXCEPT, POP_JUMP_IF_FALSE
  , SETUP_EXCEPT, SETUP_FINALLY, SETUP_LOOP, SETUP_WITH
  ]

-- | Mappings for opcodes that can be replaced with another.
equivalent :: HM.HashMap OpCode OpCode
equivalent = HM.fromList
  [ -- Only equivalent due to simplification in LOAD_METHOD.
    (CALL_METHOD, CALL_FUNCTION)
    -- These instructions load from the same category.
  , (LOAD_CLASSDEREF, LOAD_DEREF)
  , (LOAD_CLOSURE, LOAD_DEREF)
  , (LOAD_GLOBAL, LOAD_NAME)
    -- This reduces complexity.
  , (LOAD_METHOD, LOAD_ATTR)
    -- These instructions can be replaced with negations now.
  , (JUMP_IF_TRUE_OR_POP, UNARY_NOT)
  , (POP_JUMP_IF_TRUE, UNARY_NOT)
  ]
