module Language.Python.Decompile.Build.AST where

import Language.Python.Decompile.AST
import Language.Python.Decompile.Build.Transform
import Language.Python.Decompile.Clean.AST
import Language.Python.Decompile.Code

-- | Builds a clean AST from a Code structure.
buildAST :: Code -> AST
buildAST = cleanAST . buildAST'
