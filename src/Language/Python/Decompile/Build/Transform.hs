{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Build.Transform where

import Language.Python.Decompile.AST
import Language.Python.Decompile.Build.Block
import Language.Python.Decompile.Build.Statement
import Language.Python.Decompile.Clean.Block
import Language.Python.Decompile.Clean.Instruction
import Language.Python.Decompile.Code
import Language.Python.Decompile.Operation

-- | Builds a complete AST, barring final cleanup.
buildAST' :: Code -> AST
buildAST' c
  = transformAST
  . buildStatements c
  . cleanBlocks
  . buildBlocks
  . simplifyInstructions
  $ insts c

-- | Performs several transformations to fix the AST.
transformAST :: AST -> AST
transformAST
  = recMapAST removeDoubleNegations id mergeImports
  . recMapAST id buildImport id
  . recMapAST id markGlobals id
  . recMapAST id removeExceptionCleanup id
  . recMapAST id id removeDummies
  . recMapAST id id assignTuples
  . recMapAST id id fixExcepts
  . recMapAST buildMoreExpr buildMoreStmt id
  . recMapAST id id (filter (/= Pass))

-- | Builds additional statements.
buildMoreStmt :: Statement -> Statement
buildMoreStmt = buildDef . buildClass . buildDecorator

-- | Builds additional expressions.
buildMoreExpr :: Expr -> Expr
buildMoreExpr = buildLambda . buildComprehension

-- | Builds a function definition.
buildDef :: Statement -> Statement
buildDef (Assign _ (Name n) (TFunction c d))
  = Def (Name n) c d $ buildAST' c
buildDef s = s

-- | Builds a class definition.
buildClass :: Statement -> Statement
buildClass (Assign _ n (Call TBuildClass (Item (TFunction c _) : _ : is)))
  = Class n c is $ buildAST' c
buildClass s = s

-- | Builds a decorator and the decorated statement.
buildDecorator :: Statement -> Statement
buildDecorator (Assign b n (Call m [Item e]))
  | TFunction {} <- m = Assign b n $ Call m [Item e]
  | Decorator {} <- s = Decorator m s
  | Def {}       <- s = Decorator m s
  | Class {}     <- s = Decorator m s
  where s = buildMoreStmt $ Assign b n e
buildDecorator s = s

-- | Builds import statements.
buildImport :: Statement -> Statement
buildImport (Assign _ (Name s) e)
  | Just (m, n) <- extract e = Import m [(n, s)]
  where extract = \case
          Attr (TModule m') n'                    -> Just (m', n')
          Attr e' n' | Just (m', _) <- extract e' -> Just (m', n')
          _                                       -> Nothing
buildImport (Assign _ _ (TModule m)) = Import m []
buildImport s = s

-- | Merges imports from the same module.
mergeImports :: [Statement] -> [Statement]
mergeImports (Import m nns : Import m' nns' : ss)
  | m == m', nns /= [], nns' /= [], nns /= [("*", "*")], nns' /= [("*", "*")]
  = mergeImports $ Import m (nns ++ nns') : ss
mergeImports (s : ss) = s : mergeImports ss
mergeImports [] = []

-- | Builds tuple assignments.
assignTuples :: [Statement] -> [Statement]
assignTuples (s : ss)
  | Assign b (TTuple n) e <- s
  = let
      ss'        = assignTuples ss
      (es, ss'') = splitAt n ss'
      t          = Collection Tuple $ map toItem es
    in Assign b t e : ss''
  | otherwise
  = s : assignTuples ss
  where toItem (Assign _ e' TDummy) = Item e'
        toItem (Assign _ e' TStar) = ItemUnpack e'
        toItem _ = error "assignTuples: Expected dummy or star assignment."
assignTuples [] = []

-- | Replaces dummy values with the proper expressions.
removeDummies :: [Statement] -> [Statement]
removeDummies (s : ss)
  | With e Nothing (Assign _ e' TDummy : ss') <- s
  = With e (Just e') ss' : removeDummies ss
  | For TDummy e (Assign _ e' TDummy : ss') <- s
  = For e' e ss' : removeDummies ss
  | Except (Just (e, Nothing)) (Expr TDummy : Assign _ e' TDummy : ss') <- s
  = Except (Just (e, Just e')) ss' : removeDummies ss
  | otherwise
  = s : removeDummies ss
removeDummies [] = []

-- | Turns a combined except clause into proper clauses.
fixExcepts :: [Statement] -> [Statement]
fixExcepts (s : ss)
  | Except Nothing ss' <- s
  = extractExcepts ss' ++ fixExcepts ss
  | otherwise
  = s : fixExcepts ss
fixExcepts [] = []

-- | Extracts the except clauses from a series of nested conditionals.
extractExcepts :: [Statement] -> [Statement]
extractExcepts (If (Binary _ ExceptionMatch TDummy e) ss : Else ss' : ss'')
  = Except (Just (e, Nothing)) ss : extractExcepts ss' ++ ss''
extractExcepts [] = []
extractExcepts ss = [Except Nothing ss]

-- | Removes the inner try block generated for except blocks with an as.
removeExceptionCleanup :: Statement -> Statement
removeExceptionCleanup
  (Except e [Expr TDummy, Try ss, Finally (Assign _ _ None : _)])
  = Except e ss
removeExceptionCleanup s = s

-- | Adds a global annotation for all assigned to global variables.
markGlobals :: Statement -> Statement
markGlobals (Def e c p ss) | not $ null vs = Def e c p (Global vs : ss)
  where vs = concatMap getGlobal ss
markGlobals s = s

-- | Returns a list of all assigned to global variables within a function.
getGlobal :: Statement -> [Expr]
getGlobal = \case
  Assign True e _ -> [e]
  If _ a          -> concatMap getGlobal a
  Else a          -> concatMap getGlobal a
  For _ _ a       -> concatMap getGlobal a
  Try a           -> concatMap getGlobal a
  Except _ a      -> concatMap getGlobal a
  Finally a       -> concatMap getGlobal a
  With _ _ a      -> concatMap getGlobal a
  _               -> []

-- | Builds comprehensions, including generator comprehensions.
buildComprehension :: Expr -> Expr
buildComprehension (Call (TFunction c _) [Item e'])
  | [For e _ a, Return _] <- buildAST' c
  , Just (c', i, cps) <- buildCompParts a
  = Comprehension c' i $ CPFor e e' : cps
buildComprehension e = e

-- | Builds a part of a comprehension, or returns Nothing if it isn't one.
buildCompParts :: AST -> Maybe (Collection, Item, [CompPart])
buildCompParts = \case
  [TAppend c i] -> Just (c, i, [])
  [Expr (Yield e)] -> Just (Tuple, Item e, [])
  [If e a, Else _] | Just (c, i, cps) <- buildCompParts a
    -> Just (c, i, CPIf e : cps)
  [For e e' a] | Just (c, i, cps) <- buildCompParts a
    -> Just (c, i, CPFor e e' : cps)
  _ -> Nothing

-- | Builds lambda expressions.
buildLambda :: Expr -> Expr
buildLambda (TFunction c p)
  | [Return e] <- buildAST' c
  = Lambda c p e
  | [If e0 [Return e1], Else [], Return e2] <- buildAST' c
  = Lambda c p $ Conditianal e0 e1 e2
buildLambda e = e

-- | Removes double negatives introduced during decompilation.
removeDoubleNegations :: Expr -> Expr
removeDoubleNegations (Unary Not (Unary Not e)) = e
removeDoubleNegations e                         = e
