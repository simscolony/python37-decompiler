{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Build.Monad where

import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State.Strict

import Language.Python.Decompile.AST
import Language.Python.Decompile.Code

-- | The monad used when building the AST.
type Build = StateT [Expr] (ReaderT Code Identity)

-- | Runs the build monad given the related code structure.
runBuild :: Code -> Build a -> a
runBuild c = runIdentity . flip runReaderT c . flip evalStateT []

-- | Returns the nth name.
getName :: Int -> Build Name
getName n = asks $ \ Code {..} -> if n < length names
  then names !! n
  else error "getName: Invalid name index."

-- | Returns the nth variable name.
getVar :: Int -> Build Name
getVar n = asks $ \ Code {..} -> if n < length vars
  then vars !! n
  else error "getVar: Invalid variable name index."

-- | Returns the nth cell or free variable.
getCellFree :: Int -> Build Name
getCellFree n = asks $ \ Code {..} -> let cf = cell ++ free
  in if n < length cf
  then cf !! n else error "getConst: Invalid constant index."

-- | Returns the nth constant.
getConst :: Int -> Build Value
getConst n = asks $ \ Code {..} -> if n < length consts
  then consts !! n
  else error "getConst: Invalid constant index."

-- | Pushes an expression on top of the stack.
push :: Expr -> Build ()
push v = modify (v :)

-- | Pops an expression from the top of the stack.
pop :: Build Expr
pop = get >>= \case
  (v : vs) -> put vs >> return v
  []       -> return TEmptyStack

-- | Returns the complete stack.
getStack :: Build [Expr]
getStack = get

-- | Replaces the current stack with the new stack.
putStack :: [Expr] -> Build ()
putStack = put
