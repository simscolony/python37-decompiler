{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Block where

import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | A sequence of blocks.
type BlockSeq = [Block']

-- | A block wrapped with some useful information.
data Block' = Block'
  { -- | The first row number in the block.
    row' :: Int
    -- | The block itself.
  , block' :: Block
    -- | The last opcode in the block.
  , opcode' :: OpCode
    -- | Encoding of the program flow from the block.
  , target' :: Target
    -- | Indicates if the block produces any statements.
  , hasStmt' :: Bool
  } deriving (Show, Eq)

-- | Type used when re-building the structure of the code.
data Block
  -- | A simple basic block.
  = BBasic [Instruction]
  -- | A with block.
  | BWith BlockSeq
  -- | A try block, with a main clause and an except clause.
  | BExcept BlockSeq BlockSeq
  -- | A try block, with a main clause and a finally clause.
  | BFinally BlockSeq BlockSeq
  -- | An if block, with a main clause and an else clause.
  -- First argument indicates a conditional pop.
  | BIf Bool Cond BlockSeq BlockSeq
  -- | An while block.
  | BWhile Cond BlockSeq
  -- | A for block.
  | BFor BlockSeq
  deriving (Show, Eq)

-- | Type used for if and while conditions.
data Cond
  = CBlock Block'
  | CTrue
  | CNot Cond
  | CAnd Cond Cond
  | COr  Cond Cond
  deriving (Show, Eq)

-- | Type used to encode how the program flows from a block.
data Target
  = TNone
  | TNext Int
  | TJump Int
  | TBranch Int Int
  deriving (Show, Eq)

-- | Returns the non-jump row that the program may flow to, if it does.
next' :: Block' -> Maybe Int
next' = next . target'

-- | Returns the non-jump row that the program may flow to, if it does.
next :: Target -> Maybe Int
next (TNext n)     = Just n
next (TBranch n _) = Just n
next _             = Nothing

-- | Returns the jump row that the program may flow to, if it does.
jump' :: Block' -> Maybe Int
jump' = jump . target'

-- | Returns the jump row that the program may flow to, if it does.
jump :: Target -> Maybe Int
jump (TJump n)     = Just n
jump (TBranch _ n) = Just n
jump _             = Nothing
