module Language.Python.Decompile.Show.Block where

import qualified Data.Text as T

import Language.Python.Decompile.Block
import Language.Python.Decompile.Show
import Language.Python.Decompile.Show.Code

-- | Shows a sequence of blocks at the given indentation.
showBlocks :: Int -> BlockSeq -> T.Text
showBlocks i = interMap (indent i "----------\n") $ showBlock' i

-- | Shows a wrapped block at the given indentation.
showBlock' :: Int -> Block' -> T.Text
showBlock' i Block' {..} = showBlock i block'
                        <> indent i (showTarget target') <> "\n"

-- | Shows a block at the given indentation.
showBlock :: Int -> Block -> T.Text
showBlock i (BBasic is) = T.concat $ map (showInstruction i) is
showBlock i (BWith bs)
  = indent i "with:\n" <> showBlocks (i + 1) bs
showBlock i (BExcept tbs ebs)
  = indent i "try:\n" <> showBlocks (i + 1) tbs
 <> indent i "except:\n" <> showBlocks (i + 1) ebs
showBlock i (BFinally tbs fbs)
  = indent i "try:\n" <> showBlocks (i + 1) tbs
 <> indent i "finally:\n" <> showBlocks (i + 1) fbs
showBlock i (BIf _ _ ibs ebs)
  = indent i "if:\n" <> showBlocks (i + 1) ibs
 <> indent i "else:\n" <> showBlocks (i + 1) ebs
showBlock i (BWhile _ bs)
  = indent i "while:\n" <> showBlocks (i + 1) bs
showBlock i (BFor bs)
  = indent i "for:\n" <> showBlocks (i + 1) bs

-- | Shows the targets of the block.
showTarget :: Target -> T.Text
showTarget TNone          = "none"
showTarget (TNext n)      = "next: " <> tshow n
showTarget (TJump n)      = "jump: " <> tshow n
showTarget (TBranch n n') = "next: " <> tshow n <> ", jump: " <> tshow n'
