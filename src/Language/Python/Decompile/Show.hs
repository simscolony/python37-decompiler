{-# LANGUAGE Strict #-}

module Language.Python.Decompile.Show where

import qualified Data.Text as T

-- | Shows any type implementing 'Show'.
tshow :: Show a => a -> T.Text
tshow = T.pack . show

-- | Pads a string with spaces on the left to a given length.
padLeft :: Int -> T.Text -> T.Text
padLeft m s = T.replicate (m - T.length s) " " <> s

-- | Pads a string with spaces on the right to a given length.
padRight :: Int -> T.Text -> T.Text
padRight m s = s <> T.replicate (m - T.length s) " "

-- | Indents a string by four spaces for each indentation level.
indent :: Int -> T.Text -> T.Text
indent n s = T.replicate n "    " <> s

-- | Maps the function on each element, then inserts the string between each
-- element, and finally concatenates the list.
interMap :: T.Text -> (a -> T.Text) -> [a] -> T.Text
interMap s f = T.intercalate s . map f

-- | Formats a correct python tuple containing the results of mapping the given
-- function on each element.
properTuple :: (a -> T.Text) -> [a] -> T.Text
properTuple f [a] = "(" <> f a <> ",)"
properTuple f as  = "(" <> interMap ", " f as <> ")"
