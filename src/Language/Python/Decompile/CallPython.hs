{-# LANGUAGE Strict #-}

module Language.Python.Decompile.CallPython where

import Data.Maybe
import System.Directory
import System.Environment
import System.Process

import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

-- | Calls python in order to inspect the given pyc file.
-- Returns a Code structure containing the inspection results.
-- First argument is an optional python command to use.
getCodeFromPython :: Maybe FilePath -> FilePath -> IO Code
getCodeFromPython p f = do
  setupEnvironment
  p' <- maybe findPython return p
  fst . parseCode . lines <$> readProcess p' [] (script f)

-- | Tries to find the correct python command.
findPython :: IO FilePath
findPython = catMaybes <$> mapM findExecutable commands >>= \case
  (p : _) -> return p
  []      -> error "findPython: Python was not found."
  where commands = ["python3.7", "python3", "python"]

-- | Sets the environment variables needed for the python call.
setupEnvironment :: IO ()
setupEnvironment = setEnv "PYTHONIOENCODING" "UTF-8"

-- | Parses the inspection data sent from the python script.
parseCode :: [String] -> (Code, [String])
parseCode (f : nA : nK : nN : nV : nC : nF : nX : nI : ls) = (Code {..}, lsI)
  where flags         = read f
        argCount      = read nA
        kwCount       = read nK
        nNames        = read nN
        nVars         = read nV
        nCell         = read nC
        nFree         = read nF
        nConsts       = read nX
        nInsts        = read nI
        (names , lsN) = splitAt nNames ls
        (vars  , lsV) = splitAt nVars lsN
        (cell  , lsC) = splitAt nCell lsV
        (free  , lsF) = splitAt nFree lsC
        (consts, lsX) = parseValues nConsts lsF
        (ls'   , lsI) = splitAt nInsts lsX
        insts         = parseInsts 0 ls'
parseCode _ = error "parseCode: No more data."

-- | Parses the instruction data.
-- First argument is a running row count.
parseInsts :: Int -> [String] -> [Instruction]
parseInsts row (a : b : ls) = Instruction {..} : parseInsts (row + 2) ls
  where opcode   = toOpCode $ read a
        argument = read b
parseInsts _ _ = []

-- | Parses a sequence of constant values.
-- First argument is the number of values to parse.
parseValues :: Int -> [String] -> ([Value], [String])
parseValues 0 ls = ([], ls)
parseValues n ls = (v : vs, ls'')
  where (v , ls' ) = parseValue ls
        (vs, ls'') = parseValues (n - 1) ls'

-- | Parses a constant value. This includes code structures.
parseValue :: [String] -> (Value, [String])
parseValue ("code" : ls) = (VCode c, ls')
  where (c, ls') = parseCode ls
parseValue ("none" : ls) = (VNone, ls)
parseValue ("ellipsis" : ls) = (VEllipsis, ls)
parseValue ("tuple" : l : ls) = (VTuple vs, ls')
  where n         = read l
        (vs, ls') = parseValues n ls
parseValue ("frozenset" : l : ls) = (VFrozenSet vs, ls')
  where n         = read l
        (vs, ls') = parseValues n ls
parseValue (t : v : ls) = (VBasic (parseType t) v, ls)
parseValue _ = error "parseValue: No more data."

-- | Parses a basic type name.
parseType :: String -> BasicType
parseType "bool"    = Bool
parseType "int"     = Int
parseType "float"   = Float
parseType "complex" = Complex
parseType "str"     = Str
parseType "bytes"   = Bytes
parseType l         = error $ "parseValue: " ++ l ++ " not implemented."

-- | The python script to run. The file path is injected into the code.
script :: FilePath -> String
script f = unlines
  [ "import dis"
  , "import marshal"
  -- Prints a constant value.
  , "def showValue(v):"
  -- Code structures. Handled by showCode.
  , "  if hasattr(v, 'co_code'):"
  , "    print('code')"
  , "    showCode(v)"
  -- None. No data needed.
  , "  elif v == None:"
  , "    print('none')"
  -- Ellipsis. No data needed.
  , "  elif v == Ellipsis:"
  , "    print('ellipsis')"
  -- Boolean. Sends the value. Must be checked before Int.
  , "  elif isinstance(v, bool):"
  , "    print('bool')"
  , "    print(v)"
  -- Int. Sends the value.
  , "  elif isinstance(v, int):"
  , "    print('int')"
  , "    print(v)"
  -- Float. Sends the value.
  , "  elif isinstance(v, float):"
  , "    print('float')"
  , "    print(v)"
  -- Complex. Sends the value.
  , "  elif isinstance(v, complex):"
  , "    print('complex')"
  , "    print(v)"
  -- String. Sends the value in python representation.
  , "  elif isinstance(v, str):"
  , "    print('str')"
  , "    print(repr(v))"
  -- Bytes. Sends the value in python representation.
  , "  elif isinstance(v, bytes):"
  , "    print('bytes')"
  , "    print(repr(v))"
  -- Tuple. Sends the length followed by each value handled by showValue.
  , "  elif isinstance(v, tuple):"
  , "    print('tuple')"
  , "    print(len(v))"
  , "    for x in v:"
  , "      showValue(x)"
  -- Frozen set. Sends the length followed by each value handled by showValue.
  , "  elif isinstance(v, frozenset):"
  , "    print('frozenset')"
  , "    print(len(v))"
  , "    for x in v:"
  , "      showValue(x)"
  , "  else:"
  , "    raise Exception('Unhandled type: ' + str(type(v)))"
  -- Prints a Code structure.
  , "def showCode(c):"
  , "  print(c.co_flags)"
  , "  print(c.co_argcount)"
  , "  print(c.co_kwonlyargcount)"
  , "  print(len(c.co_names))"
  , "  print(len(c.co_varnames))"
  , "  print(len(c.co_cellvars))"
  , "  print(len(c.co_freevars))"
  , "  print(len(c.co_consts))"
  , "  print(len(c.co_code))"
  , "  for n in c.co_names:"
  , "    print(n)"
  , "  for n in c.co_varnames:"
  , "    print(n)"
  , "  for n in c.co_cellvars:"
  , "    print(n)"
  , "  for n in c.co_freevars:"
  , "    print(n)"
  , "  for v in c.co_consts:"
  , "    showValue(v)"
  , "  for b in c.co_code:"
  , "    print(b)"
  -- Open the pyc file and process it.
  , "with open('" ++ f ++ "','rb') as f:"
  -- Read all the contents.
  , "  b = f.read()"
  -- Use the marshal library to un-marshal the binary, skipping the header.
  , "  c = marshal.loads(b[16:])"
  -- Prints the un-marshalled Code structure to Haskell through stdout.
  , "  showCode(c)"
  ]
